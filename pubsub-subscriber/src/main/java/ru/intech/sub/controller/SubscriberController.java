package ru.intech.sub.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.intech.sub.domain.Message;
import ru.intech.sub.domain.Response;
import ru.intech.sub.service.SubscriberService;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RestController
@RequestMapping("/subscribe")
public class SubscriberController {

    private final AtomicInteger asyncCounter = new AtomicInteger(1);

    private final AtomicInteger syncCounter = new AtomicInteger(1);

    private final SubscriberService subscriberService;

    @Autowired
    public SubscriberController(SubscriberService subscriberService) {
        this.subscriberService = subscriberService;
    }

    @PostMapping("/sync")
    public @ResponseBody
    ResponseEntity<Response> acceptIncomingMessageSync(@RequestBody Message message,
                                                       @RequestParam Long delayInMs) {
        int count = asyncCounter.getAndIncrement();
        log.info(count + " sync message accepted");
        return subscriberService.proceedMessageSync(message, count, delayInMs);
    }

    @PostMapping("/async")
    public @ResponseBody
    Callable<ResponseEntity<Response>> acceptIncomingMessageAsync(@RequestBody Message message,
                                                                  @RequestParam Long delayInMs) {
        int count = syncCounter.getAndIncrement();
        log.info(count + " async message accepted");
        return subscriberService.proceedMessageAsync(message, count, delayInMs);
    }
}
