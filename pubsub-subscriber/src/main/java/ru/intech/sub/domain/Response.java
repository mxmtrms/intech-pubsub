package ru.intech.sub.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Response {

    private Long messageId;

    private Message.MessageType type;
}
