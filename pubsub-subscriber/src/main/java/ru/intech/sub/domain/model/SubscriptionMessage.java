package ru.intech.sub.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.intech.sub.domain.Message;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "subscription")
@NoArgsConstructor
public class SubscriptionMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long msisdn;

    @Column
    private Long time;

    public SubscriptionMessage(Message message) {
        this.msisdn = message.getMsisdn();
        this.time = message.getTimestamp();
    }
}
