package ru.intech.sub.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.intech.sub.domain.Message;
import ru.intech.sub.domain.model.PurchaseMessage;
import ru.intech.sub.domain.Response;
import ru.intech.sub.domain.model.SubscriptionMessage;
import ru.intech.sub.repository.PurchaseMessageRepository;
import ru.intech.sub.repository.SubscriptionMessageRepository;

import java.util.concurrent.Callable;

@Slf4j
@Service
public class SubscriberService {

    private final PurchaseMessageRepository purchaseRepository;

    private final SubscriptionMessageRepository subscriptionRepository;

    @Autowired
    public SubscriberService(PurchaseMessageRepository purchaseRepository,
                             SubscriptionMessageRepository subscriptionRepository) {
        this.purchaseRepository = purchaseRepository;
        this.subscriptionRepository = subscriptionRepository;
    }

    public ResponseEntity<Response> proceedMessageSync(Message message, Integer count, Long delayInMs) {
        log.info("Proceed " + count + " sync message");
        Long id = saveMessage(message, delayInMs);
        try {
            Thread.sleep(delayInMs);
        } catch (InterruptedException e) {
            log.error(e.toString());
        }
        return new ResponseEntity<>(new Response(id, message.getAction()), HttpStatus.OK);
    }

    public Callable<ResponseEntity<Response>> proceedMessageAsync(Message message, Integer count, Long delayInMs) {
        log.info("Proceed " + count + " async message");
        return () -> {
            Long id = saveMessage(message, delayInMs);
            return new ResponseEntity<>(new Response(id, message.getAction()), HttpStatus.OK);
        };
    }

    private Long saveMessage(Message message, Long delayInMs) {
        Long id;
        log.info("Started saving process(with delay " + delayInMs + "ms)...");
        switch (message.getAction()) {
            case PURCHASE:
                PurchaseMessage purchase = new PurchaseMessage(message);
                id = purchaseRepository.save(purchase).getId();
                break;
            case SUBSCRIPTION:
                SubscriptionMessage subscription = new SubscriptionMessage(message);
                id = subscriptionRepository.save(subscription).getId();
                break;
            default:
                throw new IllegalStateException("Action field should be one of PURCHASE or SUBSCRIPTION, but " + message.getAction());
        }

        try {
            Thread.sleep(delayInMs);
        } catch (InterruptedException e) {
            log.error(e.toString());
        }
        log.info("Saved message in database with (id)=" + id);
        log.info("Stop saving process...");
        return id;
    }
}
