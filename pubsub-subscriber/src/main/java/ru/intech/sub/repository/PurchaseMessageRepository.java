package ru.intech.sub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.intech.sub.domain.model.PurchaseMessage;

@Repository
public interface PurchaseMessageRepository extends JpaRepository<PurchaseMessage, Long> {
}
