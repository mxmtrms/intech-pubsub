package ru.intech.sub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.intech.sub.domain.model.SubscriptionMessage;

@Repository
public interface SubscriptionMessageRepository extends JpaRepository<SubscriptionMessage, Long> {
}
