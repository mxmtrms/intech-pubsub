package ru.intech.pub.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.intech.pub.domain.Message;
import ru.intech.pub.domain.Response;

import java.net.URI;
import java.util.concurrent.CompletableFuture;

@Slf4j
@EnableAsync
@Service
public class PublisherService {

    @Value("${app.subscriber.address}")
    private String address;

    @Value("${app.subscriber.port}")
    private String port;

    private final RestTemplate restTemplate;

    @Autowired
    public PublisherService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Async
    public CompletableFuture<ResponseEntity<Response>> publishAsync(Message message, Long delay)  {
        return CompletableFuture.completedFuture(restTemplate.postForEntity(getSubscriberUriByMode("async", delay), message, Response.class));
    }

    public ResponseEntity<Response> publishSync(Message message, Long delay)  {
        return restTemplate.postForEntity(getSubscriberUriByMode("sync", delay), message, Response.class);
    }

    private URI getSubscriberUriByMode(String type, Long delay) {
        return  UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(address)
                .port(Integer.parseInt(port))
                .path("/subscribe/")
                .path(type)
                .queryParam("delayInMs", delay)
                .build()
                .encode()
                .toUri();
    }
}
