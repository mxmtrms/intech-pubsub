package ru.intech.pub.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.intech.pub.domain.Message;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.LongStream;

@Slf4j
@Service
public class MessageService {

    private final Random rnd = new Random();

    public List<Message> buildRandomMessages(Long count) {
        List<Message> messages = new ArrayList<>();
        LongStream.range(0, count).forEach(i -> {
            Message message = new Message();
            message.setMsisdn(Math.abs(rnd.nextLong()));
            message.setAction(rnd.nextBoolean() ? Message.MessageType.PURCHASE : Message.MessageType.SUBSCRIPTION);
            message.setTimestamp(new Date().getTime());
            messages.add(message);
        });
        log.info("Built " + count + " random messages");
        return messages;
    }
}
