package ru.intech.pub.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.intech.pub.domain.Message;
import ru.intech.pub.domain.Response;
import ru.intech.pub.service.MessageService;
import ru.intech.pub.service.PublisherService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RestController
@RequestMapping("/publish")
public class PublisherController {

    private final PublisherService publisherService;

    private final MessageService messageService;

    private final AtomicInteger asyncCounter = new AtomicInteger(1);

    private final AtomicInteger syncCounter = new AtomicInteger(1);

    @Autowired
    public PublisherController(PublisherService publisherService, MessageService messageService) {
        this.publisherService = publisherService;
        this.messageService = messageService;
    }

    @GetMapping("/async")
    public @ResponseBody
    CompletableFuture<ResponseEntity<Response>> publishMessageAsync(@RequestParam Long delayInMs) {
        log.info("Publishing " + asyncCounter.getAndIncrement() + " message in async mode.");
        return publisherService.publishAsync(messageService.buildRandomMessages(1L).get(0), delayInMs);
    }

    @GetMapping("/sync")
    public @ResponseBody
    ResponseEntity<Response> publishMessageSync(@RequestParam Long delayInMs) {
        log.info("Publishing " + syncCounter.getAndIncrement() + " message in sync mode.");
        return publisherService.publishSync(messageService.buildRandomMessages(1L).get(0), delayInMs);
    }

    @GetMapping("/compare")
    public @ResponseBody
    ResponseEntity<String> compareAsyncAndSyncMessaging(@RequestParam Long count,
                                                        @RequestParam Long delayInMs) {
        log.info("Beginning async and sync comparision...");
        List<Message> messages = messageService.buildRandomMessages(count);
        List<Future<ResponseEntity<Response>>> asyncResults = new ArrayList<>();

        Long startAsync = new Date().getTime();
        messages.forEach(message ->
                asyncResults.add(publisherService.publishAsync(message, delayInMs))
        );
        while(!asyncResults.stream().allMatch(Future::isDone)) {}
        Long endAsync = new Date().getTime();

        Long startSync = new Date().getTime();
        messages.forEach(message -> publisherService.publishSync(message, delayInMs));
        Long endSync = new Date().getTime();

        Long asyncElapsedTime = endAsync - startAsync;
        Long syncElapsedTime = endSync - startSync;

        log.info("Request count: " + count);
        log.info("Delay: " + delayInMs + "ms");
        log.info("Elapsed time in async mode: " + asyncElapsedTime + "ms");
        log.info("Elapsed time in sync mode: " + syncElapsedTime + "ms");
        log.info("Performance: " + (syncElapsedTime - asyncElapsedTime));

        log.info("Closing async and sync comparision...");
        return ResponseEntity.ok("elapsed_time: " + (syncElapsedTime + asyncElapsedTime));
    }
}
