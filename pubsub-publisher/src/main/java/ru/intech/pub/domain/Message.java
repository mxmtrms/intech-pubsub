package ru.intech.pub.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    public enum MessageType {PURCHASE, SUBSCRIPTION};

    private Long msisdn;

    private MessageType action;

    private Long timestamp;
}
